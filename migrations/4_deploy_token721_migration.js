const NFTMarket = artifacts.require("NFTMarket");
const NFT = artifacts.require("NFT");
const fs = require('fs');

module.exports = async function (deployer) {
    const nftMarket = await deployer.deploy(NFTMarket);
    console.log("NFTMarket deployed to --> ", nftMarket.address);

    const nft = await deployer.deploy(NFT, nftMarket.address);
    console.log("NFT deployed to --> ", nft.address);

    const config = `
  export const nftmarketaddress = "${nftMarket.address}"
  export const nftaddress = "${nft.address}"
  `

    const data = JSON.stringify(config);
    fs.writeFileSync('config.js', JSON.parse(data));
};