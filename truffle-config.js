require('babel-register');
require('babel-polyfill');
const HDWalletProvider = require('@truffle/hdwallet-provider');
const {alchemyApiKey, mnemonic} = require('./secrets.json');

module.exports = {
    networks: {
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*" // Match any network id
        },
        rinkeby: {
            host: "localhost",
            provider: () => new HDWalletProvider(
                mnemonic, `https://rinkeby.infura.io/v3/${alchemyApiKey}`,
            ),
            network_id: 4,
            gas: 6700000,
        }
    },
    contracts_directory: './src/contracts',
    contracts_build_directory: './src/abis/',
    compilers: {
        solc: {
            version: "^0.8.0",
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
    }
}
