const Token = artifacts.require('Token');
const EthSwap = artifacts.require('EthSwap');

require('chai')
    .use(require('chai-as-promised'))
    .should();

function tokens(n) {
    return web3.utils.toWei(n, "ether");
}

contract('EthSwap', ([deployer, investor]) => {
    let token, ethSwap;

    before(async () => {
        token = await Token.new();
        ethSwap = await EthSwap.new(token.address);

        await token.transfer(ethSwap.address, "100000000000000000000000");
        // await token.transfer(ethSwap.address, tokens("1000000"));
    });

    describe('Token Deployment', async () => {
        it('Contract has a name', async () => {
            const name = await token.name();
            assert.equal(name, 'Test Token');
        })
    })

    describe('buyTokens', async () => {
        let result;

        before(async () => {
            result = await ethSwap.buyTokens({from: investor, value: "100000000000000000"})
        });

        it('Event', async () => {
            const event = result.logs[0].args;

            assert.equal(event.account, investor);
            assert.equal(event.token, token.address);
            assert.equal(event.amount.toString(), "10000000000000000000");
            assert.equal(event.rate.toString(), "100");
        })
    })

    describe('sellTokens', async () => {
        let result;

        before(async () => {
            result = await ethSwap.buyTokens({from: investor, value: "100000000000000000"})
        });

        it('Event', async () => {
            const event = result.logs[0].args;

            assert.equal(event.account, investor);
            assert.equal(event.token, token.address);
            assert.equal(event.amount.toString(), "10000000000000000000");
            assert.equal(event.rate.toString(), "100");
        })
    })
})