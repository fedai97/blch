import React from 'react';
import {Route, Switch} from 'react-router-dom';
import SellTokenPage from "../pages/SellToken";
import MyTokenPage from "../pages/MyToken";
import Token721Page from "../pages/Token721";

export const Router = () => {
    return (
        <main className="content">
            <Switch>
               <Route exact path="/my-token" component={MyTokenPage}/>
               <Route exact path="/sell" component={SellTokenPage}/>
               <Route exact path="/" component={Token721Page}/>
            </Switch>
        </main>
    )
};
