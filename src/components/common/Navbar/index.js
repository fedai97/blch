import React from "react";
import {useHistory} from 'react-router-dom';
import clsx from 'clsx';

import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import {makeStyles} from "@material-ui/core/styles";
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MyLocationIcon from '@material-ui/icons/MyLocation';
import {ListItemIcon} from "@material-ui/core";
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import AccessibleIcon from '@material-ui/icons/Accessible';

const useStyles = makeStyles(() => ({
    title: {
        marginLeft: '20px',
        flexGrow: 1,
    },
}));

export default function Navbar({title, account}) {
    const classes = useStyles();
    const history = useHistory();

    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const redirect = (to) => {
        switch (to) {
            case 'MTK':
                return history.push('/my-token');
            case 'SELL':
                return history.push('/sell');
            case 'TOKEN721':
                return history.push('/');
            default:
                return;
        }
    };

    const toggleDrawer = (anchor, open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({...state, [anchor]: open});
    };

    const list = (anchor) => (
        <div
            className={clsx(classes.list, {
                [classes.fullList]: anchor === 'top' || anchor === 'bottom',
            })}
            role="presentation"
            onClick={toggleDrawer(anchor, false)}
            onKeyDown={toggleDrawer(anchor, false)}
        >
            <List>
                <ListItem button onClick={() => redirect('TOKEN721')}>
                    <ListItemIcon><AccessibleIcon/></ListItemIcon>
                    <ListItemText primary={"TOKEN 721"}/>
                </ListItem>
                <ListItem button onClick={() => redirect('MTK')}>
                    <ListItemIcon><MyLocationIcon/></ListItemIcon>
                    <ListItemText primary={"MTK"}/>
                </ListItem>
                <ListItem disabled onClick={() => redirect('SELL')}>
                    <ListItemIcon><ShoppingCartIcon/></ListItemIcon>
                    <ListItemText primary={"SELL TOKEN"}/>
                </ListItem>
            </List>
        </div>
    );

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"
                            onClick={toggleDrawer('left', true)}>
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    {title}
                </Typography>
                <Button color="inherit">{account}</Button>
            </Toolbar>
            <SwipeableDrawer
                anchor={'left'}
                open={state['left']}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {list('left')}
            </SwipeableDrawer>
        </AppBar>
    )
}