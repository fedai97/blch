import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: '20px',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    input: {
        width: '100%',
    },
    buyBtn: {
        width: '100%',
    },
}));

export default function SellToken({
                                      ethBalance,
                                      tokenBalance,
                                      handleChangeFrom,
                                      handleChangeTo,
                                      handleChangeValue,
                                      transferFrom,
                                      transfer,
                                  }) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div>
                Balance [ETH]: {window.web3.utils.fromWei(ethBalance, 'Ether')}
            </div>
            <div>
                Balance [MTK]: {window.web3.utils.fromWei(tokenBalance, 'Ether')}
            </div>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Paper className={classes.paper}>
                        <TextField id="outlined-basic" label="To" variant="outlined" className={classes.input}
                                   onChange={handleChangeTo}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paper}>
                        <TextField id="outlined-basic" label="Amount" variant="outlined" className={classes.input}
                                   onChange={handleChangeValue}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" className={classes.buyBtn} onClick={transfer}>
                        Transfer
                    </Button>
                </Grid>

                <Grid item xs={4}>
                    <Paper className={classes.paper}>
                        <TextField id="outlined-basic" label="From" variant="outlined" className={classes.input}
                                   onChange={handleChangeFrom}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paper}>
                        <TextField id="outlined-basic" label="To" variant="outlined" className={classes.input}
                                   onChange={handleChangeTo}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={4}>
                    <Paper className={classes.paper}>
                        <TextField id="outlined-basic" label="Amount" variant="outlined" className={classes.input}
                                   onChange={handleChangeValue}
                        />
                    </Paper>
                </Grid>

                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" className={classes.buyBtn} onClick={transferFrom}>
                        Transfer from
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
};
