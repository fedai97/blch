import React from 'react';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: '20px',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    input: {
        width: '100%',
    },
    buyBtn: {
        width: '100%',
    },
}));

export default function Token721({
                                     ethBalance,
                                     tokenBalance,
                                     handleChangeTo,
                                     handleChangeValue,
                                     transfer,
                                 }) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <div>
                Balance [ETH]: {window.web3.utils.fromWei(ethBalance, 'Ether')}
            </div>
            <div>
                Balance [MTK]: {window.web3.utils.fromWei(tokenBalance, 'Ether')}
            </div>
        </div>
    )
};
