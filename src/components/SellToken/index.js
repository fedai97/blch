import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: '20px',
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    input: {
        width: '100%',
    },
    buyBtn: {
        width: '100%',
    },
}));

export default function SellToken({
                                      ethBalance,
                                      tokenBalance,
                                      handleChangeEthToken,
                                      etherAmount,
                                      buyTokens,
                                  }) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        <div>
                            Balance: {window.web3.utils.fromWei(ethBalance, 'Ether')}
                        </div>
                        <TextField id="outlined-basic" label="EthToken" variant="outlined" className={classes.input}
                                   onChange={handleChangeEthToken}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={6}>
                    <Paper className={classes.paper}>
                        <div>
                            Balance: {window.web3.utils.fromWei(tokenBalance, 'Ether')}
                        </div>
                        <TextField id="outlined-basic" label="Token" variant="outlined" className={classes.input}
                                   value={etherAmount}
                        />
                    </Paper>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" className={classes.buyBtn} onClick={buyTokens}>
                        Buy
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
};
