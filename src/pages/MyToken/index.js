import React from "react";
import {makeStyles} from "@material-ui/core/styles";

import MyToken from "../../components/MyToken";
import Navbar from "../../components/common/Navbar";
import useInitMyTokenWeb3 from "../../hooks/web3/useInitMyTokenWeb3";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function MyTokenPage() {
    const classes = useStyles();

    const initMyTokenWeb3 = useInitMyTokenWeb3();

    return <div className={classes.root}>
        <Navbar account={initMyTokenWeb3.account} title={'MTK'}/>
        {
            initMyTokenWeb3.loaded
            && <MyToken {...initMyTokenWeb3}/>
        }
    </div>
}