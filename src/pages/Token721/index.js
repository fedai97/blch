import React from "react";
import {makeStyles} from "@material-ui/core/styles";

import Navbar from "../../components/common/Navbar";
import useInitToken721Web3 from "../../hooks/web3/useInitToken721Web3";
import Token721 from '../../components/Token721';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function SellTokenPage() {
    const classes = useStyles();

    const initWeb3 = useInitToken721Web3();

    return <div className={classes.root}>
        <Navbar account={initWeb3.account} title={'Token721'}/>
        {
            initWeb3.loaded
            && <Token721 {...initWeb3}/>
        }
    </div>
}