import {useEffect, useState} from "react";
import Web3 from 'web3';
import MyToken from '../../abis/MyToken.json';

export default function useInitMyTokenWeb3() {
    const [account, setAccount] = useState('');
    const [ethBalance, setEthBalance] = useState('');
    const [token, setToken] = useState('');
    const [tokenBalance, setTokenBalance] = useState('');
    const [loaded, setLoaded] = useState(false);

    const [from, setFrom] = useState('');
    const [to, setTo] = useState('');
    const [value, setValue] = useState(0);

    const loadWeb3 = async () => {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            await window.ethereum.enable();
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert("Non ethereum!")
        }
    };

    const loadBlockchainData = async () => {
        const web3 = window.web3;

        const accounts = await web3.eth.getAccounts();
        setAccount(accounts[0]);

        const ethBalance = await web3.eth.getBalance(accounts[0]);
        setEthBalance(ethBalance);

        const networkID = await web3.eth.net.getId();
        const tokenData = MyToken.networks[networkID];
        if (tokenData) {
            const token = new web3.eth.Contract(MyToken.abi, tokenData.address);
            const tokenBalance = await token.methods.balanceOf(accounts[0]).call();

            setToken(token);
            setTokenBalance(tokenBalance.toString());
            setLoaded(true);
        } else {
            alert('Contract [MyToken] not deployed');
        }
    };

    const handleChangeFrom = (e) => setFrom(e.target.value);
    const handleChangeTo = (e) => setTo(e.target.value);
    const handleChangeValue = (e) => setValue(e.target.value);

    const transfer = () => {
        token.methods.transfer(to, value)
            .send({from: account})
            .on('transitionHash', (hash) => {

            });
    };

    const transferFrom = () => {
        token.methods.transferFrom(from, to, value)
            .send({from: account})
            .on('transitionHash', (hash) => {

            });
    };

    useEffect(() => {
        (async function () {
            await loadWeb3();
            await loadBlockchainData();
        })()
    }, []);

    return {
        account, ethBalance, token, tokenBalance, loaded,
        handleChangeFrom, handleChangeTo, transferFrom, handleChangeValue, transfer
    }
}