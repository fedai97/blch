import {useEffect, useState} from "react";
import Web3 from 'web3';
import Token from '../../abis/Token.json';
import EthSwap from '../../abis/EthSwap.json';

export default function useInitWeb3() {
    const [account, setAccount] = useState('');
    const [ethBalance, setEthBalance] = useState('');
    const [token, setToken] = useState('');
    const [tokenBalance, setTokenBalance] = useState('');
    const [ethSwap, setEthSwap] = useState('');
    const [loaded, setLoaded] = useState(false);

    const [etherAmount, setEtherAmount] = useState(0);

    const loadWeb3 = async () => {
        if (window.ethereum) {
            window.web3 = new Web3(window.ethereum);
            await window.ethereum.enable();
        } else if (window.web3) {
            window.web3 = new Web3(window.web3.currentProvider)
        } else {
            window.alert("Non ethereum!")
        }
    };

    const loadBlockchainData = async () => {
        const web3 = window.web3;

        const accounts = await web3.eth.getAccounts();
        setAccount(accounts[0]);

        const ethBalance = await web3.eth.getBalance(accounts[0]);
        setEthBalance(ethBalance);

        const networkID = await web3.eth.net.getId();
        const tokenData = Token.networks[networkID];
        if (tokenData) {
            const token = new web3.eth.Contract(Token.abi, tokenData.address);
            const tokenBalance = await token.methods.balanceOf(accounts[0]).call();

            setToken(token);
            setTokenBalance(tokenBalance.toString())
        } else {
            alert('Token not deployed contract');
        }

        const ethSwapData = EthSwap.networks[networkID];
        if (ethSwapData) {
            const ethSwap = new web3.eth.Contract(EthSwap.abi, ethSwapData.address);
            setEthSwap(ethSwap);
        } else {
            alert('EthSwap not deployed contract');
        }

        setLoaded(true);
    };

    const handleChangeEthToken = (e) => {
        setEtherAmount(e.target.value * 100);
    };

    const buyTokens = () => {
        ethSwap.methods.buyTokens()
            .send({value: etherAmount, from: account})
            .on('transitionHash', (hash) => {

            });
    };

    useEffect(() => {
        (async function () {
            await loadWeb3();
            await loadBlockchainData();
        })()
    }, []);

    return {
        etherAmount,
        account, ethBalance, token, tokenBalance, ethSwap, loaded,
        handleChangeEthToken, buyTokens,
    }
}